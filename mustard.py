# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

S = '00:01:07,400-234-090\n00:05:01,701-080-080\n00:05:00,400-234-090'


def longest_call_for_free(call_log):
    """ Longest calls may have multiple in case of race condition """
    longest_call = max([i['duration'] for i in call_log])
    calls = [c for c in call_log if c['duration'] == longest_call]
    default_number = calls[0]['phone_number']

    if len(calls) > 1:
        for n in calls:
            if n < default_number:
                default_number = n

    # Make call free
    for i in call_log:
        if i['phone_number'] == default_number:
            i['cost'] = 0
            break

    return call_log


def get_phone_call_cost(duration):
    """ Return a price in cents based on duration passed in. """
    MAX_PROMO_TARIFF_MINUTES = 5

    if duration <= 0:
        return 0

    price = 3
    if duration > MAX_PROMO_TARIFF_MINUTES * 60:
        price = 5

    return duration * price


def parse_phone_log_entry(log):
    """
    Parses a phone call log entry.
    :param entry: string - e.g.:  '00:01:07,400-234-090'
    :return: dict
    """
    entry = {
        'duration': 0,
        'phone_number': None,
        'cost': 0
    }

    call_details = log.split(',')
    entry['duration'] = sum(s * int(n) for s, n in zip([3600, 60, 1], call_details[0].split(":")))
    entry['phone_number'] = call_details[1].replace('-', '')
    entry['cost'] = get_phone_call_cost(entry['duration'])

    return entry


def solution(S):
    calls = [parse_phone_log_entry(n) for n in S.split('\n')]
    calls = longest_call_for_free(calls)

    cost = 0

    for c in calls:
        cost += c['cost']

    return cost
